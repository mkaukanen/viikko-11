/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.event.MouseAdapter;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;



/**
 *
 * @author Miki
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private AnchorPane AP;
    int pointsClicked=0;
    double startX, startY, endX, endY;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }  
   
    @FXML
    private void addCircle(MouseEvent event) {
        Point p = new Point(event); 
        int exists = -1;
        ShapeHandler sh = ShapeHandler.getsh();
        ArrayList<Point> PointList = sh.getPointList();
        for (Point point : PointList){
            if (point.c.getCenterX()+ point.c.getRadius()>=event.getX() 
                && point.c.getCenterX()- point.c.getRadius()<=event.getX()
                && point.c.getCenterY()+ point.c.getRadius()>=event.getY()
                && point.c.getCenterY()- point.c.getRadius()<=event.getY()){
                exists=1;  // ^ sweep the area of the dot, not only the center point
            }
        }
        if (exists ==1){  //if hit in the a dot's area, trigger greeting
                System.out.println("Hei, olen "+p.name+"!");
                drawLine(event);

        }
        else{
            System.out.println("(Dot added to map)");
            AP.getChildren().add(p.c);
            sh.addPoint(p);
        }

    
    }
        
    private void drawLine(MouseEvent event){
        if (pointsClicked==2){
            pointsClicked=0;
        }    
        if (pointsClicked==1){
            endX = event.getX();
            endY = event.getY();
            ShapeHandler sh = ShapeHandler.getsh();
            ArrayList<Line> LineList = sh.getLineList();
            for (Line line : LineList){
                line.setVisible(false);
            }
            Point p = new Point(startX, startY, endX, endY);
            AP.getChildren().add(p.l);
            System.out.println("Line drawed");
            sh.addLine(p.l);
            pointsClicked=2;
        }
        if (pointsClicked==0){
            startX = event.getX();
            startY= event.getY();
            pointsClicked=1;
        }  
         
    }
    

}
    


