
import java.util.ArrayList;
import javafx.scene.shape.Line;


/**
 *
 * @author Miki
 */
public class ShapeHandler {
    private ArrayList<Point> PointList = new ArrayList<>();
    private ArrayList<Line> LineList = new ArrayList<>();
    static private ShapeHandler sh = null;
    
    private ShapeHandler(){
        
    }
    public static synchronized ShapeHandler getsh(){
        if (sh == null){
            sh=new ShapeHandler();
        }
        return sh;
    }
    
    public void addPoint(Point p){
        PointList.add(p);
    }
    
    public ArrayList getPointList(){
        return PointList;
    }
   
    public void addLine(Line l){
        LineList.add(l);
    }
    
    public ArrayList getLineList(){
        return LineList;
    }
    
}
