
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Miki
 */
public class Point {
    String name="";
    Circle c;   
    Line l;
 
    public Point(MouseEvent event){
        Float radius = 5.0f;
        c = new Circle(event.getX(), event.getY(), radius, Color.BLUE);  
        name = "piste";
                               
}
    public Point(double startX, double startY, double endX, double endY){
        l = new Line(startX, startY, endX, endY);
        l.setFill(Color.RED);
        name = "line";
    }

    
}
